import 'package:http/http.dart' as http;

class ApiService {
  static final client = http.Client();
  static const int timeout = 240000;
  static String baseUrl = "https://api-aslis.angkitagro.com/api";
}
